package com.jakubbrzozowski.githubbrowser

import android.app.Application
import com.jakubbrzozowski.githubbrowser.injection.component.ApplicationComponent
import com.jakubbrzozowski.githubbrowser.injection.component.DaggerApplicationComponent
import com.jakubbrzozowski.githubbrowser.injection.module.ApplicationModule
import timber.log.Timber

class MyApplication : Application() {
    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }
}