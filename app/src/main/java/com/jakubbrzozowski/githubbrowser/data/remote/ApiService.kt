package com.jakubbrzozowski.githubbrowser.data.remote

import com.jakubbrzozowski.githubbrowser.data.model.Repositories
import com.jakubbrzozowski.githubbrowser.data.model.User
import com.jakubbrzozowski.githubbrowser.data.model.Users
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    companion object {
        const val URL = "https://api.github.com"
    }

    @GET("/search/repositories")
    fun searchRepositories(
            @Query("q") q: String,
            @Query("sort") sort: String,
            @Query("order") order: String,
            @Query("page") page: Int
    ): Single<Repositories>

    @GET("/search/users")
    fun searchUsers(
            @Query("q") q: String,
            @Query("sort") sort: String,
            @Query("order") order: String,
            @Query("page") page: Int
    ): Single<Users>

    @GET("/users/{login}")
    fun getUserDetails(
            @Path("login") login: String
    ): Single<User?>
}
