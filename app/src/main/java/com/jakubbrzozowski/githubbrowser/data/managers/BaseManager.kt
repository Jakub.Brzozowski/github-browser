package com.jakubbrzozowski.githubbrowser.data.managers

abstract class BaseManager {
    companion object {
        const val DEFAULT_SORT = "id"
        const val DEFAULT_ORDER = "asc"
    }
}