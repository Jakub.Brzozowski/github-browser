package com.jakubbrzozowski.githubbrowser.data.managers

import com.jakubbrzozowski.githubbrowser.data.model.User
import com.jakubbrzozowski.githubbrowser.data.remote.ApiService
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class UsersManager @Inject
constructor(private val apiService: ApiService) : BaseManager() {

    private var usersCache: MutableList<User?> = Collections.emptyList()

    open fun searchUsers(q: String, page: Int): Single<List<User?>?> {
        if (page == CombinedUsersAndReposManager.STARTING_PAGE) {
            usersCache = Collections.emptyList()
        }
        return apiService.searchUsers(q, DEFAULT_SORT, DEFAULT_ORDER, page)
                .map { repos ->
                    repos.items?.let {
                        if (usersCache.isEmpty()) {
                            usersCache = it.toMutableList()
                        } else {
                            usersCache.addAll(it)
                        }
                        return@map repos.items
                    }
                }
                .subscribeOn(Schedulers.io())
    }

    open fun getUserById(id: Int?): User? {
        return usersCache.find { it?.id?.equals(id) == true }
    }

    open fun getFollowersCount(login: String): Single<Int?> {
        return apiService.getUserDetails(login)
                .map { user -> user.followers }
                .subscribeOn(Schedulers.io())
    }
}