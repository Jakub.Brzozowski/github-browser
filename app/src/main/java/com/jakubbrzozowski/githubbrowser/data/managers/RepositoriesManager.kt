package com.jakubbrzozowski.githubbrowser.data.managers

import com.jakubbrzozowski.githubbrowser.data.model.Repository
import com.jakubbrzozowski.githubbrowser.data.remote.ApiService
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class RepositoriesManager @Inject
constructor(private val apiService: ApiService) : BaseManager() {

    open fun searchRepositories(q: String, page: Int): Single<List<Repository?>?> {
        return apiService.searchRepositories(q, DEFAULT_SORT, DEFAULT_ORDER, page)
                .map { repos -> repos.items }
                .subscribeOn(Schedulers.io())
    }
}