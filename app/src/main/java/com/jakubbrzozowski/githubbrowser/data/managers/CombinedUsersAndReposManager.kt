package com.jakubbrzozowski.githubbrowser.data.managers

import com.jakubbrzozowski.githubbrowser.data.model.HasId
import com.jakubbrzozowski.githubbrowser.data.model.Repository
import com.jakubbrzozowski.githubbrowser.data.model.User
import com.jakubbrzozowski.githubbrowser.utils.PositionAndCount
import io.reactivex.Single
import io.reactivex.SingleSource
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class CombinedUsersAndReposManager @Inject
constructor(private val repositoriesManager: RepositoriesManager, private val usersManager: UsersManager) : BaseManager() {

    companion object {
        const val STARTING_PAGE = 1
    }

    private var searchResults: MutableList<HasId?> = Collections.emptyList()
    private var pageCounter = STARTING_PAGE

    open fun loadNextPage(q: String): Single<PositionAndCount> {
        return Single.create { emitter ->
            val lastPosition = searchResults.size - 1
            searchRepositoriesAndUsers(q, ++pageCounter)
                    .subscribe(
                            { _ ->
                                emitter.onSuccess(PositionAndCount(
                                        lastPosition,
                                        searchResults.size - lastPosition - 1))
                            },
                            { ex -> Timber.e(ex) })
        }
    }

    open fun searchRepositoriesAndUsers(q: String): Single<List<HasId?>?> {
        pageCounter = STARTING_PAGE
        searchResults = Collections.emptyList()
        return searchRepositoriesAndUsers(q, STARTING_PAGE)
    }

    protected open fun searchRepositoriesAndUsers(q: String, page: Int): Single<List<HasId?>?> {
        return Single.zip<List<Repository?>?, List<User?>?, List<HasId?>?>(
                repositoriesManager.searchRepositories(q, page)
                        .onErrorResumeNext { _ -> SingleSource { Collections.emptyList<HasId>().toMutableList() } },
                usersManager.searchUsers(q, page)
                        .onErrorResumeNext { _ -> SingleSource { Collections.emptyList<HasId>().toMutableList() } },
                BiFunction { t1, t2 -> zipReposWithUsers(t1, t2) })
                .subscribeOn(Schedulers.io())
    }

    protected open fun zipReposWithUsers(reposList: List<Repository?>, usersList: List<User?>):
            List<HasId?> {
        val combinedList = reposList + usersList
        val sortedList = combinedList.sortedBy { presentableOnMainRecycler -> presentableOnMainRecycler?.id }
        if (searchResults.isEmpty()) {
            searchResults = sortedList.toMutableList()
        } else {
            searchResults.addAll(sortedList)
        }
        return searchResults
    }
}