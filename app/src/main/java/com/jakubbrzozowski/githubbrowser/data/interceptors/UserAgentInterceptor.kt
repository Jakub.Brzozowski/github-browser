package com.jakubbrzozowski.githubbrowser.data.interceptors

import android.support.annotation.NonNull
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class UserAgentInterceptor(private val mUserAgent: String) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(@NonNull chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder().header("User-Agent", mUserAgent).build()
        return chain.proceed(request)
    }
}