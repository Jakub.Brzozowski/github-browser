package com.jakubbrzozowski.githubbrowser.data.model

interface HasId {
    val id: Int?
}