package com.jakubbrzozowski.githubbrowser.injection.component

import android.app.Application
import android.content.Context
import com.jakubbrzozowski.githubbrowser.data.managers.CombinedUsersAndReposManager
import com.jakubbrzozowski.githubbrowser.data.managers.RepositoriesManager
import com.jakubbrzozowski.githubbrowser.data.managers.UsersManager
import com.jakubbrzozowski.githubbrowser.data.remote.ApiService
import com.jakubbrzozowski.githubbrowser.injection.module.ApiModule
import com.jakubbrzozowski.githubbrowser.injection.module.ApplicationModule
import com.jakubbrzozowski.githubbrowser.injection.qualifier.ApplicationContext
import com.jakubbrzozowski.githubbrowser.injection.qualifier.MainScheduler
import dagger.Component
import io.reactivex.Scheduler
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class, ApiModule::class))
interface ApplicationComponent {

    @ApplicationContext
    fun context(): Context

    fun application(): Application
    fun apiService(): ApiService
    fun usersManager(): UsersManager
    fun repositoriesManger(): RepositoriesManager
    fun combinedUsersAndReposManager(): CombinedUsersAndReposManager
    @MainScheduler
    fun scheduler(): Scheduler
}