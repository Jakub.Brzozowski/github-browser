package com.jakubbrzozowski.githubbrowser.injection.qualifier

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class MainScheduler
