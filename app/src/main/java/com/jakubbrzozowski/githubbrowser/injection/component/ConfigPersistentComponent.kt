package com.jakubbrzozowski.githubbrowser.injection.component

import com.jakubbrzozowski.githubbrowser.injection.module.ActivityModule
import com.jakubbrzozowski.githubbrowser.injection.scope.ConfigPersistent
import dagger.Component

@ConfigPersistent
@Component(dependencies = arrayOf(ApplicationComponent::class))
interface ConfigPersistentComponent {
    fun activityComponent(activityModule: ActivityModule): ActivityComponent
}