package com.jakubbrzozowski.githubbrowser.injection.component

import com.jakubbrzozowski.githubbrowser.injection.module.ActivityModule
import com.jakubbrzozowski.githubbrowser.injection.scope.PerActivity
import com.jakubbrzozowski.githubbrowser.ui.main.MainActivity
import com.jakubbrzozowski.githubbrowser.ui.userdetails.UserDetailsActivity
import dagger.Subcomponent

@PerActivity
@Subcomponent(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(userDetailsActivity: UserDetailsActivity)
}