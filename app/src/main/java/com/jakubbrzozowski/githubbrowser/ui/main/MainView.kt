package com.jakubbrzozowski.githubbrowser.ui.main

import com.jakubbrzozowski.githubbrowser.data.model.HasId
import com.jakubbrzozowski.githubbrowser.ui.base.MvpView

interface MainView : MvpView {

    fun showRepos(repos: List<HasId?>?)
    fun updateRepos(firstPos: Int, count: Int)
    fun openUserDetails(userId: Int)
    fun getQueryString(): String
}