package com.jakubbrzozowski.githubbrowser.ui.main

import com.jakubbrzozowski.githubbrowser.data.managers.CombinedUsersAndReposManager
import com.jakubbrzozowski.githubbrowser.injection.qualifier.MainScheduler
import com.jakubbrzozowski.githubbrowser.injection.scope.ConfigPersistent
import com.jakubbrzozowski.githubbrowser.ui.base.BasePresenter
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

@ConfigPersistent
class MainPresenter
@Inject
constructor(private val combinedUsersAndReposManager: CombinedUsersAndReposManager,
            @MainScheduler private val mainScheduler: Scheduler) : BasePresenter<MainView>() {

    private val uiSubscriptions = CompositeDisposable()

    init {
        allSubscriptions.add(uiSubscriptions)
    }

    private fun onUserClicked(userId: Int) {
        view.openUserDetails(userId)
    }

    private fun loadMoreSearchResults() {
        val queryString = view.getQueryString()
        if (!queryString.isBlank()) {
            subsciprtions.add(combinedUsersAndReposManager.loadNextPage(queryString)
                    .observeOn(mainScheduler)
                    .subscribe(
                            { it -> view.updateRepos(it.firstPos, it.count) },
                            { ex -> Timber.e(ex) })
            )
        }
    }

    fun onRecyclerAdapterChanged(viewClickSubject: PublishSubject<Int>?,
                                 endOfListReachedSubject: PublishSubject<Int>?) {
        uiSubscriptions.clear()
        viewClickSubject?.subscribe { onUserClicked(it) }?.let { uiSubscriptions.add(it) }
        endOfListReachedSubject?.subscribe { loadMoreSearchResults() }?.let { uiSubscriptions.add(it) }
    }

    fun searchFieldChanged(searchQuery: String?) {
        if (!searchQuery.isNullOrBlank()) {
            subsciprtions.clear()
            subsciprtions.add(
                    combinedUsersAndReposManager.searchRepositoriesAndUsers(searchQuery.toString())
                            .observeOn(mainScheduler)
                            .subscribe(
                                    { list -> view.showRepos(list) },
                                    { ex -> Timber.e(ex) }))
        }
    }
}