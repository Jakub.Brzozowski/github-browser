package com.jakubbrzozowski.githubbrowser.ui.userdetails

import com.jakubbrzozowski.githubbrowser.data.managers.UsersManager
import com.jakubbrzozowski.githubbrowser.data.model.User
import com.jakubbrzozowski.githubbrowser.injection.qualifier.MainScheduler
import com.jakubbrzozowski.githubbrowser.injection.scope.ConfigPersistent
import com.jakubbrzozowski.githubbrowser.ui.base.BasePresenter
import io.reactivex.Scheduler
import timber.log.Timber
import javax.inject.Inject

@ConfigPersistent
open class UserDetailsPresenter
@Inject
constructor(private val usersManager: UsersManager,
            @MainScheduler private val mainScheduler: Scheduler) : BasePresenter<UserDetailsView>() {

    fun userIdReceived(userId: Int?) {
        usersManager.getUserById(userId)?.let {
            view.showUserDetails(it)
            getFollowersCount(it)
        }
    }

    protected open fun getFollowersCount(user: User) {
        subsciprtions.add(
                usersManager.getFollowersCount(user.login.toString())
                        .observeOn(mainScheduler)
                        .subscribe(
                                { count -> count?.let { it1 -> view.showFollowersCount(it1) } },
                                { ex -> Timber.e(ex) }))
    }
}