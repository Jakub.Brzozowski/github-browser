package com.jakubbrzozowski.githubbrowser.ui.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.jakubbrzozowski.githubbrowser.R
import com.jakubbrzozowski.githubbrowser.data.model.HasId
import com.jakubbrzozowski.githubbrowser.data.model.Repository
import com.jakubbrzozowski.githubbrowser.data.model.User
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_repository.view.*
import kotlinx.android.synthetic.main.item_user.view.*

class SearchResultsRecyclerViewAdapter(private val items: List<HasId?>) :
        RecyclerView.Adapter<SearchResultsRecyclerViewAdapter.ViewHolder>() {
    companion object {
        const val TYPE_USER = 1
        const val TYPE_REPOSITORY = 2
        const val UNSUPPORTED_TYPE_EXCEPTION_MSG = "Unsupported HasId implementation"
    }

    val userClickSubject = PublishSubject.create<Int>()
    val endOfListReachedSubject = PublishSubject.create<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View?
        when (viewType) {
            TYPE_USER -> view = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
            TYPE_REPOSITORY -> view = LayoutInflater.from(parent.context).inflate(R.layout.item_repository, parent, false)
            else -> throw IllegalArgumentException(UNSUPPORTED_TYPE_EXCEPTION_MSG)
        }
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        items[position]?.let { holder.bind(it, userClickSubject) }
        if (position.equals(items.size - 2)) {
            endOfListReachedSubject.onNext(0)
        }
    }

    override fun getItemViewType(position: Int): Int {
        when (items[position]) {
            is Repository -> return TYPE_REPOSITORY
            is User -> return TYPE_USER
            else -> throw IllegalArgumentException(UNSUPPORTED_TYPE_EXCEPTION_MSG)
        }
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: HasId, clickPublishSubject: PublishSubject<Int>) {
            when (item) {
                is Repository -> {
                    view.repoTitle.text = item.fullName
                    view.setOnClickListener { }
                }
                is User -> {
                    view.item_username.text = item.login
                    RxView.clicks(view)
                            .map { item.id }
                            .subscribe(clickPublishSubject)
                }
            }
        }
    }
}