package com.jakubbrzozowski.githubbrowser.ui.userdetails

import android.os.Bundle
import com.jakubbrzozowski.githubbrowser.R
import com.jakubbrzozowski.githubbrowser.data.model.User
import com.jakubbrzozowski.githubbrowser.ui.base.BaseActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_user_details.*
import javax.inject.Inject

class UserDetailsActivity : BaseActivity(), UserDetailsView {
    companion object {
        const val USER_ID_EXTRA_KEY = "userId"
    }

    @Inject
    lateinit var presenter: UserDetailsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent.inject(this)
        setContentView(R.layout.activity_user_details)

        presenter.attachView(this)

        val userId = intent?.getIntExtra(USER_ID_EXTRA_KEY, 0)
        presenter.userIdReceived(userId)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun showUserDetails(user: User) {
        details_username.text = user.login
        Picasso.with(this)
                .load(user.avatarUrl)
                .into(details_avatar)
    }

    override fun showFollowersCount(count: Int) {
        details_followers.text = getString(R.string.followers_count, count)
    }
}
