package com.jakubbrzozowski.githubbrowser.ui.userdetails

import com.jakubbrzozowski.githubbrowser.data.model.User
import com.jakubbrzozowski.githubbrowser.ui.base.MvpView

interface UserDetailsView : MvpView {
    fun showUserDetails(user: User)
    fun showFollowersCount(count: Int)
}