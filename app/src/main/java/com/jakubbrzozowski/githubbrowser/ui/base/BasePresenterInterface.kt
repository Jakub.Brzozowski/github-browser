package com.jakubbrzozowski.githubbrowser.ui.base

interface BasePresenterInterface<in V : MvpView> {
    fun attachView(view: V)
    fun detachView()
}