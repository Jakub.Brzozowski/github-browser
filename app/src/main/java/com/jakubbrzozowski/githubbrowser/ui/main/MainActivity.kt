package com.jakubbrzozowski.githubbrowser.ui.main

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import com.jakubbrzozowski.githubbrowser.R
import com.jakubbrzozowski.githubbrowser.data.model.HasId
import com.jakubbrzozowski.githubbrowser.ui.base.BaseActivity
import com.jakubbrzozowski.githubbrowser.ui.userdetails.UserDetailsActivity
import com.jakubbrzozowski.githubbrowser.ui.userdetails.UserDetailsActivity.Companion.USER_ID_EXTRA_KEY
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainView {
    @Inject
    lateinit var presenter: MainPresenter
    private var recyclerViewAdapter: SearchResultsRecyclerViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent.inject(this)
        setContentView(R.layout.activity_main)

        presenter.attachView(this)
        mainRecycler.layoutManager = LinearLayoutManager(this)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                presenter.searchFieldChanged(newText)
                return true
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun showRepos(repos: List<HasId?>?) {
        mainRecycler.adapter = repos?.let {
            recyclerViewAdapter = SearchResultsRecyclerViewAdapter(it)
            presenter.onRecyclerAdapterChanged(recyclerViewAdapter?.userClickSubject,
                    recyclerViewAdapter?.endOfListReachedSubject)
            recyclerViewAdapter
        }
    }

    override fun updateRepos(firstPos: Int, count: Int) {
        recyclerViewAdapter?.notifyItemRangeInserted(firstPos, count)
    }

    override fun openUserDetails(userId: Int) {
        val intent = Intent(this, UserDetailsActivity::class.java)
        intent.putExtra(USER_ID_EXTRA_KEY, userId)
        startActivity(intent)
    }

    override fun getQueryString(): String {
        return searchView.query.toString()
    }
}
