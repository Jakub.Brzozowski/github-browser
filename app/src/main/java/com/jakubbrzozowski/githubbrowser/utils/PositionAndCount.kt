package com.jakubbrzozowski.githubbrowser.utils

data class PositionAndCount(val firstPos: Int, val count: Int)