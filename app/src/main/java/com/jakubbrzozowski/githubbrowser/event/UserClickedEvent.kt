package com.jakubbrzozowski.githubbrowser.event

data class UserClickedEvent(val userId: Int?)