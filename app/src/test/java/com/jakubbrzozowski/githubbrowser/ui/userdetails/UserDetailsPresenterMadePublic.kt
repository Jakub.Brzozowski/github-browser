package com.jakubbrzozowski.githubbrowser.ui.userdetails

import com.jakubbrzozowski.githubbrowser.data.managers.UsersManager
import com.jakubbrzozowski.githubbrowser.data.model.User
import io.reactivex.Scheduler

class UserDetailsPresenterMadePublic(usersManager: UsersManager,
                                     mainScheduler: Scheduler)
    : UserDetailsPresenter(usersManager , mainScheduler) {

    public override fun getFollowersCount(user: User) {
        super.getFollowersCount(user)
    }
}