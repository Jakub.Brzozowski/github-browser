package com.jakubbrzozowski.githubbrowser.ui.main

import com.jakubbrzozowski.githubbrowser.data.managers.CombinedUsersAndReposManager
import com.jakubbrzozowski.githubbrowser.data.model.HasId
import com.jakubbrzozowski.githubbrowser.event.LoadMoreSearchResultsEvent
import com.jakubbrzozowski.githubbrowser.event.UserClickedEvent
import com.jakubbrzozowski.githubbrowser.utils.PositionAndCount
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner.Silent::class)
class MainPresenterTest {

    @Mock
    lateinit var combinedUsersAndReposManager: CombinedUsersAndReposManager

    @Mock
    lateinit var view: MainView

    lateinit var presenter: MainPresenter

    @Before
    fun setup() {
        presenter = MainPresenter(combinedUsersAndReposManager, Schedulers.trampoline())
        presenter.attachView(view)
    }

    @Test
    fun onUserClickedEvent_nonNullUserId_dontOpenUserDetails() {
        val event = UserClickedEvent(3)

        presenter.onEvent(event)

        verify(view, times(1)).openUserDetails(anyInt())
    }

    @Test
    fun onLoadMoreSearchResultsEvent_loadNextPageSuccess_updateRepos() {
        val firstPos = 3
        val count = 8
        val query = "query"
        doReturn(query).`when`(view).getQueryString()
        doReturn(Single.just(PositionAndCount(firstPos, count)))
                .`when`(combinedUsersAndReposManager)
                .loadNextPage(query)

        presenter.onEvent(LoadMoreSearchResultsEvent())

        verify(view, times(1)).updateRepos(firstPos, count)
    }

    @Test
    fun onLoadMoreSearchResultsEvent_loadNextPageError_dontUpdateRepos() {
        val query = "query"
        doReturn(query).`when`(view).getQueryString()
        doReturn(Single.error<Throwable>(Throwable("")))
                .`when`(combinedUsersAndReposManager)
                .loadNextPage(query)

        presenter.onEvent(LoadMoreSearchResultsEvent())

        verify(view, times(0)).updateRepos(anyInt(), anyInt())
    }

    @Test
    fun onLoadMoreSearchResultsEvent_queryBlank_dontLoadNextPage() {
        val query = " "
        doReturn(query).`when`(view).getQueryString()
        doReturn(Single.just(PositionAndCount(0, 0)))
                .`when`(combinedUsersAndReposManager)
                .loadNextPage(query)
        
        presenter.onEvent(LoadMoreSearchResultsEvent())

        verify(combinedUsersAndReposManager, times(0)).loadNextPage(query)
    }

    @Test
    fun onLoadMoreSearchResultsEvent_queryNotBlank_loadNextPage() {
        val query = "query"
        doReturn(query).`when`(view).getQueryString()
        doReturn(Single.just(PositionAndCount(0, 0)))
                .`when`(combinedUsersAndReposManager)
                .loadNextPage(query)

        presenter.onEvent(LoadMoreSearchResultsEvent())

        verify(combinedUsersAndReposManager, times(1)).loadNextPage(query)
    }

    @Test
    fun searchFieldChanged_searchRepositoriesAndUsersSuccess_showRepos() {
        val list = Collections.emptyList<HasId>()
        val query = "query"
        doReturn(query).`when`(view).getQueryString()
        doReturn(Single.just(list))
                .`when`(combinedUsersAndReposManager)
                .searchRepositoriesAndUsers(query)

        presenter.searchFieldChanged(query)

        verify(view, times(1)).showRepos(list)
    }

    @Test
    fun searchFieldChanged_searchRepositoriesAndUsersError_dontshowRepos() {
        val query = "query"
        doReturn(query).`when`(view).getQueryString()
        doReturn(Single.error<Throwable>(Throwable("")))
                .`when`(combinedUsersAndReposManager)
                .searchRepositoriesAndUsers(query)

        presenter.searchFieldChanged(query)

        verify(view, times(0)).showRepos(Collections.emptyList())
    }

    @Test
    fun searchFieldChanged_queryBlank_dontsearchRepositoriesAndUsers() {
        val list = Collections.emptyList<HasId>()
        val query = " "
        doReturn(query).`when`(view).getQueryString()
        doReturn(Single.just(list))
                .`when`(combinedUsersAndReposManager)
                .searchRepositoriesAndUsers(query)

        presenter.searchFieldChanged(query)

        verify(combinedUsersAndReposManager, times(0)).searchRepositoriesAndUsers(query)
    }

    @Test
    fun searchFieldChanged_queryNotBlank_searchRepositoriesAndUsers() {
        val list = Collections.emptyList<HasId>()
        val query = "query"
        doReturn(query).`when`(view).getQueryString()
        doReturn(Single.just(list))
                .`when`(combinedUsersAndReposManager)
                .searchRepositoriesAndUsers(query)

        presenter.searchFieldChanged(query)

        verify(combinedUsersAndReposManager, times(1)).searchRepositoriesAndUsers(query)
    }

}