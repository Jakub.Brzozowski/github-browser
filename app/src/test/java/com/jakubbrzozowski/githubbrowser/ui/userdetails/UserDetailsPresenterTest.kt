package com.jakubbrzozowski.githubbrowser.ui.userdetails

import com.jakubbrzozowski.githubbrowser.data.managers.UsersManager
import com.jakubbrzozowski.githubbrowser.data.model.User
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner.Silent::class)
class UserDetailsPresenterTest {

    @Mock
    lateinit var usersManager: UsersManager

    @Mock
    lateinit var view: UserDetailsView

    lateinit var presenter: UserDetailsPresenterMadePublic

    @Before
    fun setup() {
        presenter = UserDetailsPresenterMadePublic(usersManager, Schedulers.trampoline())
        presenter.attachView(view)
    }

    @Test
    fun onGetUserByIdReturnsNotNullUser_showUserDetails() {
        val username = "testuser"
        val userId = 3
        val user = User(id = userId, login = username)
        doReturn(user)
                .`when`(usersManager)
                .getUserById(userId)
        doReturn(Single.just(0))
                .`when`(usersManager)
                .getFollowersCount(username)

        presenter.userIdReceived(userId)

        verify(view, times(1)).showUserDetails(user)
    }

    @Test
    fun onGetUserByIdReturnsNotNullUser_getFollowersCount() {
        val username = "testuser"
        val userId = 3
        val user = User(id = userId, login = username)
        doReturn(user)
                .`when`(usersManager)
                .getUserById(userId)
        doReturn(Single.just(0))
                .`when`(usersManager)
                .getFollowersCount(username)

        presenter.userIdReceived(userId)

        verify(usersManager, times(1)).getFollowersCount(username)
    }

    @Test
    fun onGetFollowersCountSuccessReturnsNotNullCount_showFollowersCount() {
        val username = "testuser"
        val count = 42
        val user = User(login = username)
        doReturn(Single.just(count))
                .`when`(usersManager)
                .getFollowersCount(username)

        presenter.getFollowersCount(user)

        verify(view, times(1)).showFollowersCount(count)
    }
}