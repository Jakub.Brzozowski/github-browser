package com.jakubbrzozowski.githubbrowser.data.managers

import com.jakubbrzozowski.githubbrowser.data.model.Repository
import com.jakubbrzozowski.githubbrowser.data.model.User
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner.Silent::class)
class CombinedUsersAndReposManagerTest {

    @Mock
    lateinit var usersManager: UsersManager

    @Mock
    lateinit var repositoriesManager: RepositoriesManager

    lateinit var manager: CombinedUsersAndReposManagerPublic

    @Before
    fun setup() {
        manager = CombinedUsersAndReposManagerPublic(repositoriesManager, usersManager)
    }

    @Test
    fun searchRepositoriesAndUsers_searchRepositoriesReturnsError_doesNotReturnError() {
        val query = "testquery"
        val page = 5
        Mockito.doReturn(Single.just(listOf(User())))
                .`when`(usersManager)
                .searchUsers(query, page)
        Mockito.doReturn(Single.error<Throwable>(Throwable()))
                .`when`(repositoriesManager)
                .searchRepositories(query, page)

        val testObserver = manager.searchRepositoriesAndUsers(query, page).test()

        testObserver.assertNoErrors()
    }

    @Test
    fun searchRepositoriesAndUsers_searchUsersReturnsError_doesNotReturnError() {
        val query = "testquery"
        val page = 5
        Mockito.doReturn(Single.error<Throwable>(Throwable()))
                .`when`(usersManager)
                .searchUsers(query, page)
        Mockito.doReturn(Single.just(listOf(Repository())))
                .`when`(repositoriesManager)
                .searchRepositories(query, page)

        val testObserver = manager.searchRepositoriesAndUsers(query, page).test()

        testObserver.assertNoErrors()
    }

    @Test
    fun zipReposWithUsers_returnsCombinedListSortedById() {
        val repo1 = Repository(id = 1)
        val repo2 = Repository(id = 3)
        val repo3 = Repository(id = 5)
        val user1 = User(id = 2)
        val user2 = User(id = 4)
        val repos = listOf(repo1, repo2, repo3)
        val users = listOf(user1, user2)

        val zipped = manager.zipReposWithUsers(repos, users)

        assert(zipped == listOf(repo1, user1, repo2, user2, repo3))
    }
}