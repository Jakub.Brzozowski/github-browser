package com.jakubbrzozowski.githubbrowser.data.managers

import com.jakubbrzozowski.githubbrowser.data.model.HasId
import com.jakubbrzozowski.githubbrowser.data.model.Repository
import com.jakubbrzozowski.githubbrowser.data.model.User
import io.reactivex.Single

class CombinedUsersAndReposManagerPublic(repositoriesManager: RepositoriesManager,
                                         usersManager: UsersManager)
    : CombinedUsersAndReposManager(repositoriesManager, usersManager) {

    public override fun searchRepositoriesAndUsers(q: String, page: Int): Single<List<HasId?>?> {
        return super.searchRepositoriesAndUsers(q, page)
    }

    public override fun zipReposWithUsers(reposList: List<Repository?>, usersList: List<User?>): List<HasId?> {
        return super.zipReposWithUsers(reposList, usersList)
    }
}